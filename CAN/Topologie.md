Plusieurs topologies réseaux peuvent être appliquées à un bus CAN  :
- Etoile
- Chainée
- Circulaire
- Double circulaire
L'objectif de cette page est de décrire et tester, si le temps nous le permet, ces différentes topologies. La section [câblage](##Câblage) spécifie les caractéristiques des câbles utilisés par le projet.
# Etoile
Tous les contrôleurs sont connectés à un même noeud. Si un câble se rompt, cela n'interrompt pas l'ensemble du réseau.

Avantages :
- Une deconnexion n'affecte pas le réseau
Inconvenients :
- Difficulté dans la propagation des signaux
- Nombre de contrôleurs limités par le nombre de connecteurs disponibles sur le noeud
- Requiert un PCB dédié pour le noeud
# Chainée
Les contrôleurs sont connectés les uns à la suite des autres.

Avantages :
- Une "infinité" de contrôleurs peuvent se connecter au réseau
- Simplicité de mise en place
- Facilité d'ajouter un nouveau contrôleur
Inconvénients :
- Une deconnexion peut provoquer la chute partielle ou totale du réseau car une terminaison devient manquante
# Circulaire
Comme une topologie chainée mais les extremités du réseau sont connectées entre elles.

Avantages :
- Pas besoin de terminaisons
- Une déconnexion ne pause pas de problème car les données "passent de l'autre côté" de la boucle
Inconvénients :
- Besoin d'un long câble pour relier les deux extrémités
# Double circulaire
Deux topologie circulaires l'une à coté de l'autre.

Avantages :
- Robustesse
Inconvénients :
- Beaucoup de câbles
# Câblage
Chaque câble du réseau est une paire différentielle torsadée. Les spécifités de notre réseau sont les suivantes :
CANH : Bleu
CANL : Vert
Type de câble : AWG24 twisted multi strand copper wire pair
Connecteur : JST PHR-2
Sertissage : SPH-002T-P0.5L
Receptacle : JST S2B-PH-K
Longueur maximale d'un câble : 30 centimètres
### Problèmes rencontrés
Les câbles sont très fragiles au niveau des connecteurs. Plusieurs solutions pourraient servir à améliorer la qualité des connexions :
1. Fabriquer des câbles de meilleure facture ce qui signifie développer une méthode fiable et répétable.
2. Reculer les connecteurs à l'intérieur du PCB pour qu'ils soient plus rigides.
3. Espacer un peu plus les deux connecteurs pour faciliter la déconnexion des câbles lorsque les deux réceptacles sont peuplés.