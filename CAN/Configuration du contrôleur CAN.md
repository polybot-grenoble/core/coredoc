# Introduction
Le but ici est de trouver une configuration permettant d'avoir le même débit entre les F4 et les L4, tout en respectant cette liste de débits : 
- 10 Kbits/s
- 20 Kbits/s
- 50 Kbits/s
- 100 Kbits/s
- 125 Kbits/s
- 250 Kbits/s
- 500 Kbits/s
- 800 Kbits/s
- 1000 Kbits/s
([Bringing CAN interface up - eLinux.org](https://elinux.org/Bringing_CAN_interface_up))

**ATTENTION :** Les paramètres décrits dans ce document sont expérimentaux et peuvent avoir évolué depuis le début du projet. Merci de vous référer aux dernières configuration en date sur le [dépôt Gitlab](https://gitlab.com/polybot-grenoble/core/can-prototype-stm32).

# Timing
Un des challenge lors de la configuration du CAN est de définir les différents paramètres qui contrôlent le bit timing. Ces derniers ont une influence directe sur le débit et la fiabilité du contrôleur configuré. Les contrôleurs d'un même réseau doivent tous avoir le même débit mais pas forcément les même paramètres (Ils dépendent en partie de la vitesse d'horloge du microcôntroleur). Ces paramètres sont les suivants :
- Sync Jump Width
- Bit segment 1
- Bit segment 2
- Prescaler

Le manuel de référence de la STM32 L4 précise les calculs à effectuer pour configurer ces paramètres. Un extrait est disponible ci-dessous.

![](Pasted%20image%2020240315192533.png)*Figure : Paramètres de timing. Extrait du manuel de référence L4 (rm0394).*

Pour plus de détails voir section 44.7.7 du manuel de référence L4 (rm0394). 
# Recherche de paramètres via CubeMX
Une autre méthode, que nous avons préféré, consiste à calculer ces paramètres à l'aide de CubeM, l'outil de configuration fourni par STMicroelectronics. 
## STM32 F446Re

![](CubeMX_CAN_F4.png)

On a :
- Prescaler: 2
- Time Quanta in Bit Segment 1: 12 Times
- Time Quanta in Bit Segment 2: 8 Times
- ReSynchronization Jump Width: 1 Time

## SMT32 L432Kc

![](CubeMX_CAN_L4.png)

Résumé : 
- Prescaler: 8
- Time Quanta in Bit Segment 1: 2 Times
- Time Quanta in Bit Segment 2: 1 Times
- ReSynchronization Jump Width: 1 Time