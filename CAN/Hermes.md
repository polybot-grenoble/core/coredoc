# Introduction

Le protocole de communication se base sur le standard CAN2.0B, dont les frames possèdent un ID étendu (29 bits). Si nécessaire, il est possible d'utiliser un adressage restreint à 11 bits sur la norme CAN2.0A.

# Répartition de l'espace des IDs 

Pour simplifier l'attribution des identifiants, on va donner un identifiant (entier sur n bits) à chaque périphérique, un identifiant (sur p bits) , et un bit permettant de savoir si il s'agit d'une demande ou une réponse.

## Adressage sur 29 bits

Sur un mot 32 bits on a:
`0b 000d dddd dddc cccc cccc cccs ssss sssq`

En détail:
- q : Demande = 0 / Réponse = 1
- d : Destinataire (8 bits = 256 périphériques)
- s : Source (8 bits)
- c : Commande (12 bits = 4096 commandes / périphérique)

## Adressage sur 11 bits

Sur un mot 16 bits on a:
`0b 0000 0ddd cccc sssq`

En détail:
- q : Demande = 0 / Réponse = 1
- d : Destinataire (3 bits = 8 périphériques)
- s : Source (3 bits)
- c : Commande (4 bits = 16 commandes / périphérique)

# Message type Broadcast

L'ID `0xff` (ou `0x7` sur 11 bits) sera réservé pour les messages ayant pour destinataire l'ensemble des périphériques. 

# Spécificateurs de format

Chaque donnée à transmettre doit être associée à un spécificateur de format pour qu'elles puissent être interpretées à la réception.

| Spécificateur | Type associé |
| ------------- | ------------ |
| `c`           | `char`       |
| `o`           | `int8_t`     |
| `x`           | `int16_t`    |
| `i`           | `int32_t`    |
| `l`           | `int64_t`    |
| `f`           | `float`      |
| `d`           | `double`     |
| `s`           | `char *`     |

# Procédures système obligatoires

Les procédures ayant un identifiant compris entre 4095 et 4080 (sur 29 bits) sont réservés pour le système et ne doivent pas être supplantées par l'utilisateur.

## Liste des procédures réservées

| Procédure | Nom | Paramètre (Question) | Paramètre (Réponse) | Description | 
| --------- | ----| -------------------- | ------------------- | ----------- |
| 4095      | Heartbeat       | _rien_ | _rien_   | Message permettant de détecter la présence d'un périphérique |
| 4094      | Status          | _rien_ | Partiel(Statut) | Demande de statut *(à définir)* |
| 4093      | Partial_Begin   | ID de la commande, Longueur (en octets) du contenu transmis | _rien_ | Débute une transmission de contenu partiel |
| 4092      | Partial_Content | _Contenu Partiel_ | _pas de réponse_ | Envoie un segment de contenu partiel |
| 4091      | Signal | Signal | Etat | Envoie un signal à la machine à état du périphérique et renvoie le nouvel Etat |
| 4090      | Log | Partiel(Message) | rien | Envoie un log dans la console du robot
| 4089      | Error | code (2o), procedure (2o) | rien | Envoie un code d'erreur 
| 4088      | | |
| 4087      | | |
| 4086      | | |
| 4085      | | |
| 4084      | | |
| 4083      | | |
| 4082      | | |
| 4081      | | |
| 4080      | | |

_NB: il y a une différence entre **rien** et **pas de réponse** : **rien** signifie qu'un message est envoyé mais ne contient pas de message, tandis que **pas de réponse** indique que celui qui reçoit ne répond pas à celui qui envoie._

_NB: La notation **Partiel(...)** indique qu'un contenu partiel sera envoyé ultérieurement comme réponse._

## Codes d'erreur

Il existe 16 codes d'erreur génériques pouvant être combinés pour en former de plus complexes.

| Code   | Signification |
| ------ | ------------- |
| 0x1    | Erreur Inconnue |
| 0x2    | ID de procédure invalide |
| 0x4    | Argument invalide |
| 0x8    | 
| 0x10   |
| 0x20   | 
| 0x40   |
| 0x80   |
| 0x100  |
| 0x200  |
| 0x400  |
| 0x800  |
| 0x1000 |
| 0x2000 |
| 0x4000 |
| 0x8000 |

## Description du fonctionnement du protocole

### Eléments utilisés

Le protocole repose sur les buffers d'entrée et de sortie, et l'ordre d'envoi des messages est géré par une FIFO.

### Les buffers 

Que ce soit pour la transmission ou la réception, les données seront stockées octets par octets dans des tableaux possédant un compteur de longueur intégrée.

```cpp
/** Partial buffer pointer with length and position tracking */
typedef struct {

    /** Buffer's associated command */
    uint16_t command;
    
    /** Length of the buffer */
    uint16_t len;

    /** Current in-buffer position */
    uint16_t pos;
    
    /** Pointer to the buffer */
    uint8_t data[HERMES_MAX_BUFFER_LEN];

} HermesBuffer;
```

De plus la structure contient une indication sur la commande associée.

### La FIFO

Chaque message étant pourra être envoyé de façon partielle si il fait plus de 8 octets. Pour autant si on doit envoyer un "gros" message, il ne faut pas bloquer l'envoi de messages système plus importants.

C'est pourquoi Hermes utilise une FIFO circulaire où une donnée peut être insérée comme sur une pile.

```cpp
typedef struct {
    
    /** Head of the FIFO */
    uint8_t head;

    /** Tail of the FIFO */
    uint8_t tail;
    
    /** Data in the FIFO */
    uint16_t data[16];

} HermesFIFO;
```

### Envoi d'un message

#### Message court

Un message court est défini comme ayant une charge utile d'au plus 8 octets. Le message est envoyé tel quel sans prévenir de la taille de la charge au préalable, l'indication du nombre d'octets transmis se trouvant déjà dans la structure d'une trame CAN.
#### Message long

Un message long est défini comme ayant une charge utile d'au moins 9 octets. Dans ce cas, une trame `Partial_Begin` annonce récepteur qu'il va recevoir une charge utile de `len` octets pour la commande `command`. Ensuite, la charge utile est envoyée par paquets d'au plus 7 octets muni d'un octet indiquant au récepteur quelle est la commande correspondant à ce tronçon (i.e. `[ command, ...data ]`), l'indication du nombre d'octets transmis se trouvant déjà dans la structure d'une trame `Partial_Content`.
#### Dans les 2 cas

Une fois le message envoyé, le buffer d'envoi est considéré détruit (i.e. `pos == len`) et peut être alloué de nouveau pour envoyer un autre message.

### Réception d'un message

#### Message court

Lors de la réception d'une trame CAN, qu'elle contienne des données ou non, un buffer de réception est alloué. Si la trame ne contient pas de données, le buffer se voit allouer une unique valeur nulle, et ce pour se placer dans la configuration `len=1`, `pos=1` permettant de signaler l'arrivée de la commande. En effet `pos=0` correspond à un buffer libre (jamais utilisé ou récemment consommé). 
#### Message long

Lors de la réception d'une trame `Partial_Begin`, un buffer est réservé avec comme longueur et commande les paramètres contenus dans la trame. Cette trame sera suivie de trames `Partial_Content` contenant un octet désignant l'ID de la commande transmise et au plus 7 octets devant être ajoutés au buffer.
#### Dans les 2 cas

Une fois un buffer complété (i.e. `pos == len`), la commande est déclarée comme reçue et l'utilisateur en est avertit **dans le programme principal** lors de la vérification de l'état des buffers. 

Une fois le buffer consommé par l'utilisateur, sa tête de réception est remise à zéro (i.e. `pos=0`) pour l'indiquer comme vide.
### Schéma d'éxécution

#### Envoi

Lorsqu'une commande est ajoutée à la FIFO d'envoi, et que celle-ci est vide, cela déclenche une première écriture sur le bus. A la fin de cet envoi, on vérifie si l'ensemble des messages devant être envoyés le sont, si oui, on s'arrête, si non, on continue d'écrire sur le bus.

Si une commande à envoyer est ajoutée à la FIFO non vide, le message n'est pas envoyé et attend son tour.

A noter qu'en cas de polling, on va essayer de lire le bus entre 2 envois. On considère ainsi la réception prioritaire sur l'envoi dans ce cas précis.

#### Réception

Lorsqu'un buffer se complète, cela signifie qu'on a reçu une commande, ainsi l'utilisateur est avertit et peut consommer le buffer, une seule et unique fois par réception.

Cette détection de complétion et cet avertissement se font dans **le programme principal** utilisant Hermes, cela permet dans le cas d'un périphérique de pouvoir être réinitialisé par interruption en cas de boucle infinie créée par l'utilisateur, et dans le cas de Core Linux de synchroniser la disponibilité des données de manière prédictible.



