## Niveaux de tension
Le support physique le plus répandu pour utiliser le protocole CAN est la paire différentielle. Chaque bit d'une donnée conduit simultanément le niveau de tension de deux lignes électriques, le plus souvent torsadées, de manière à obtenir une différence de tension entre celles-ci. Ces lignes sont nommées CAN HIGH et CAN LOW et ont chacune des niveaux de tensions différents pour un même bit. Ces niveaux dépendent du transmetteur utilisé. Dans le cas du MCP2542FD on retrouve les niveaux suivants :

| Bit | Tension CANH (V) | Tension CANL (V) |
| --- | ---------------- | ---------------- |
| 0   | 2,75             | 0,5              |
| 1   | 4,5              | 2,25             |
L'information peut alors reconvertie en signal digital par un autre transmetteur qui mesurera le potentiel électrique entre les deux lignes. Si le potentiel est inférieur à un certain niveau, le bit obtenu vaut 0 (état récessif), s'il est supérieur à un niveau plus important, le bit vaut 1 (état dominant).  Si l'on reprend le MCP2542FD, l'état récessif est compris entre -500mV et 50mV et l'état dominant entre 1,5V et 3V. Cette méthode à l'avantage de rendre la ligne de transmission très résiliante aux interférences car le bruit se verrait fortement attenués lorsque l'on mesure la différence de potentiel.
## Terminaisons
Pour garantir la bonne propagation des signaux électriques, les deux extremités de la paire différentielle doivent être équipées de terminaisons résistives pour éviter la propagation d'ondes réflechies. Ces terminaisons apparaissent le plus souvent comme une résistance de 120 Ohm.

Nous avons décidé de suivre les recommandations de Microchip lors de la conception de nos terminaisons. Celle-ci sont situées au plus proche du transmetteur et respectent le schéma suivant :
![](Pasted%20image%2020240328205314.png)
Les transmetteurs sont situés sur l'ensemble de la ligne mais uniquement les extremités doivent être équipées d'une terminaison. J3 et J4 sont des jumper qui peuvent être activés ou non pour permettre cela. Du point de vue de la paire différentielle les deux résistances en série équivalent bien à une unique résistance de 120 Ohm. Nous avons cependant ajouté une capacité de 4700pF entre les deux. Ce montage agit comme un filtre passe-bas qui filtre les bruits HF qui peuvent être présents sur la paire. Ce montage s'est avéré efficace jusqu'à présent. Il requiert cependant d'avoir des valeurs de résistance les plus égales possibles pour éviter un déséquilibre en courant entre les deux lignes.