Nous remercions chaleureusement :
- Mr Sylvain Toru, pour ses conseils et son suivi
- Mme Nathalie Guyadier, pour ses conseils en gestion de projet
- Mr Christophe Brunschweiler et Mr Pierre Ficheux pour leurs conseils sur le temps réel
- L'association Polybot Grenoble, qui nous permet de financer ce projet
- Les membres de Polybot Grenoble, pour leur enthousiasme et esprit critique