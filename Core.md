Core est une abstraction logicielle de la composition du robot qui uniformise la communication et le fonctionnement de chaque partie.

Ce document a pour objectif de documenter le développement du logiciel afin que les futures générations puissent le reprendre et l'améliorer.

Core a pour objectif d'horizontaliser la communication entre les différents composants du robot.(cf. Figure 1)

![](architecture_globale.png)
_Figure 1 : Empreinte de Core dans l'architecture du robot_
## Sommaire

- CAN
	- [Configuration du contrôleur CAN](Configuration%20du%20contrôleur%20CAN.md)
	- [Hermes](CAN/Hermes.md)
	- [Topologie](CAN/Topologie.md)
	- [Niveaux de tension et terminaisons](CAN/Niveaux%20de%20tension%20et%20terminaisons)
- Core Linux
	- [Guide de démarrage rapide](Guide%20de%20démarrage%20rapide.md)
	- [Cockpit](Cockpit.md)
	- [Bugs connus](Bugs%20connus)
	- [SPI-CAN Controller](SPI-CAN%20Controller.md)
	- [Recherches contrôleur](Recherches%20contrôleur.md)
	- [Fichier de configuration](Fichier%20de%20configuration.md)
	- [Configuration Temps Réel](Configuration%20Temps%20Réel.md)
- Core STM
	- [Guide de démarrage rapide Core STM](Guide%20de%20démarrage%20rapide%20Core%20STM.md)
	- [Logiciel Core STM](Logiciel%20Core%20STM.md)
	- [CAN Transceiver](Core%20STM/CAN%20Transceiver.md)
	- [Adaptation STM32 F446Re](Core%20STM/Adaptation%20STM32%20F446Re.md)
	- [Recherches en vrac](Core%20STM/Recherches%20en%20vrac.md)
- Talos
	- [Gigatalos](Talos/Gigatalos.md)
	- [Talos périphérique](Talos/Talos%20p%C3%A9riph%C3%A9rique)
- [Remerciements](Remerciements.md)
## Livrables
- Shield Raspberry PI 4 CAN
- Shield L4 CAN
- Shield F4 CAN
- Câbles
- Prototype transmetteur CAN
- Prototype contrôleur CAN
- Prototype réseau étoile