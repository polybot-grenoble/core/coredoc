## Introduction
Gigatalos est une machine à états. Elle est implémentée dans la Raspberry Pi et est chargée de gérer la bonne exécution de la stratégie durant le match. Les fichiers C++ définissant l'objet *gigaTalos* sont `gigaTalos.cpp` et `gigaTalos.hpp`. 
## Description de la machine à états
### Diagramme d'états
Les différents états et transitions de gigaTalos sont définis sur la figure ci-dessous :
![[Pasted image 20240325154501.png]]
<div align="center">Diagramme d'états de gigaTalos</div>

Sur la figure ci-dessus on peut constater que, dans son fonctionnement général, la machine à états est configurée pour exécuter une stratégie implémentée. Des états sont prévus au cas où un périphérique ne serait plus détecté, ou encore si une erreur est remontée au système.
### Attributs privés
L'objet gigaTalos est composé de deux variables membres privées.
La variable `currentState` contient l'état actuel où est la machine à états.
La variable `command` contient la dernière commande exécutée par la machine à états. Cette variable peut être récupérée via la fonction getter `commands getCurrentCommand()`.
L'objet contient aussi une fonction privée `void nextState()` qui permet de passer à l'état suivant en fonction de la commande contenue dans `command`.
### Attributs protégés
L'objet gigaTalos contient une variable protégée `tabTransit`, un vecteur contenant des structures `transitions`. Cette structure est composée d'un état dit de départ, une commande et un état d'arrivée. Cette structure permet de coder les transitions de la machine à états. Si gigaTalos est à un état A et que `tabTransit` contient une transition {ETAT_A, commande1, ETAT_B}, alors l'usage de la fonction `void changeCommand(commands newCommand)` avec en paramètre `commande1`, va faire passer gigaTalos à l'état B lors du prochain appel à la fonction `void exec()` décrite ci-après.
Ce tableau est rempli dans le constructeur de gigaTalos par appel de la fonction membre `void push_back(transitions transition)`.
### Attributs publics
L'objet gigaTalos contient plusieurs fonctions membres publiques permettant de manipuler la machine à états.
+ La fonction `void exec()` va avancer à l'état suivant et exécuter la fonction virtuelle associée.
+ La fonction `void changeCommand(commands newCommand)` permet de donner à la machine à états la prochaine commande à exécuter.
+ La fonction `commands getCurrentCommand()` permet de récupérer la dernière commande exécutée par gigaTalos.
+ Enfin les fonctions virtuelles ont pour vocation d'être écrasées par des fonctions codées par l'utilisateur. Il y a une fonction virtuelle pour chaque état, qui a pour vocation a être exécutée lorsque gigaTalos arrive dans un nouvel état.
## Comment modifier gigaTalos
/!\\ Si jamais vous voulez modifier la machine à états, nous vous recommandons d'en parler d'abord à un membre de la team Core.
### Ajout/retrait d'un état
+ Dans le fichier `gigaTalos.hpp` :
Commencer par ajouter l'état dans l'enum `states{}`. Si une fonction virtuelle doit être exécutée lors de l'arrivée dans cette fonction, il faut la définir dans la partie publique de gigaTalos.

+ Dans le fichier `gigaTalos.cpp` :
Il faut ajouter dans le tableau `tabTransit` les états et commandes permettant d'arriver à votre nouvel état. Pour cela il faut utiliser la fonction membre `push_back(transitions transition)` pour y ajouter les transitions.
Dans la fonction `void exec()` il faut ajouter un case dans le switch pour votre état. Dans ce case il faudra faire un appel à la fonction virtuelle redéfinie. 
