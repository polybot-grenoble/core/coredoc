## Introduction
La machine à états Talos a pour but de commander les états d'un périphérique du robot. Cela permet à un utilisateur voulant coder son périphérique de l'inscrire dans des blocs prédéfinis, qui seront ensuite exécutés par commande via le  protocole CAN.
## Utilisation
Pour implémenter du code utilisateur dans la machine à états, il faut réécrire les fonctions précédées du code `__attribute__((weak))` dans le fichier *Talos_functionsUser.h* en y ajoutant les parties de code qui correspondent à ces 3 blocs :
+ Initialisation : *Talos_initialisation*
+ Boucle d'exécution : *Talos_loop*
+ Protocole d'erreur : *Talos_onError*
+ Protocole de déconnexion : *Talos_onDisconnected*
## Modification du code
Ces instructions ne sont nécessaires que si vous voulez modifier le fonctionnement de la machine à états. Veuillez vous référer à un membre de l'équipe Core avant toute modification.
### Ajout d'un état
Pour ajouter un état vous devez la définir dans le typedef enum `Talos_states` dans le fichier *Talos_periphMain.h*.
Il faudra ensuite ajouter les transitions menant et partant de cet état dans le tableau `tabTransit` du fichier *Talos_periphMain.c*. Ne pas oublier de mettre à jour la taille du tableau, à la fois dans sa définition et dans l'initialisation de la variable `Talos->n` dans la fonction `Talos_init`.
Enfin, toujours dans *Talos_periphMain.c*, il faudra modifier la fonction `Talos_exec` pour y rajouter un case dans le switch exécutant le code correspondant à votre état.
### Ajout d'une commande
Pour ajouter une commande vous devez la définir dans le typedef enum `Talos_commands` dans le fichier *Talos_periphMain.h*.
Il faudra ensuite ajouter les transitions exécutées par cette commande dans le tableau `tabTransit` du fichier *Talos_periphMain.c*. Ne pas oublier de mettre à jour la taille du tableau, à la fois dans sa définition et dans l'initialisation de la variable `Talos->n` dans la fonction `Talos_init`.