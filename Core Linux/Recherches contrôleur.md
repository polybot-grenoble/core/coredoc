La carte fille du Pi4 permet de fournir les connecteurs pour interfacer le bus du robot ainsi que certains composants essentiels comme l'IHM (et potentiellement le LiDAR). Certains connecteurs ne pourront pas être importés sur la carte fille, en particulier l'alimentation.
## Cahier des charges
La carte doit inclure les fonctionnalités suivantes :
- Connecteur CAN avec selecteur de terminaisons
- Connecteur IHM et Lidar (UART)
- Connecteur I2C
- LED d'état programmable
## Prototype
Un prototype de la carte fille a été réalisé afin de valider le fonctionnement du contrôleur CAN. Celui-ci est critique car il permet la communication entre le Pi4 et le bus CAN du robot.
### Matériel existant
Polybot possède actuellement un HAT CAN qui comprend un MCP2515FD, un transmetteur VP230 ainsi que les composants essentiels au bon fonctionnement de ces puces. Ce HAT a cependant ses limites. On ne peut pas lire les signaux entre le controleur et le transmetteur ni changer de transmetteur. Nous souhaitons cependant utiliser un autre type de transmetteur, qui utilise une paire différentielle avec une amplitude de 5V et non 3.3V.
Ce HAT servira uniquement pour tester la partie logiciel du CAN, en attendant que le matériel soit conçu et validé.

Notre choix s'est donc porté vers le controleur CAN MCP2518FD, version plus moderne du MCP2515FD. Le tout accompagné d'un transmetteur MCP2544WFDT également utilisé par les STM. Ce controleur requiert une horloge externe précise. Comme proposé par la datasheet nous allons choisir les composants adaptés à la réalisation d'un oscillateur de Pierce.
### Oscillateur de Pierce
Un oscillateur de Pierce est composé d'une partie active (propre à la puce) et d'une partie active représentées dans le schéma ci-dessous. L'objectif d'un tel oscillateur est de fournir au système un signal d'horloge stable, dans notre cas d'une fréquence de 40 MHz. Le concepteur a donc le choix des composants passifs, c-à-d le quartz, les condensateurs de charge et la résistance en série. Le choix de ces composants est primordial pour que l'oscillateur démarre et se stabilise autour de sa fréquence d'oscillation tout en étant résiliant aux perturbations externes.

![](pierce_oscillator.png)
*Figure 1 : Schéma haut niveau de l'oscillateur de Pierce* 
#### Quartz
Le premier composant à choisir est le Quartz. Celui-ci doit être choisi par rapport à sa fréquence de résonance, sa capacité de charge et sa résistance équivalente en série. Un intervalle de transconductance $g_m$ du circuit est recommandé par le constructeur de la puce :
$$2040µA/V \le g_{m_{40}} \le 3060µA/V $$
**EN THEORIE**, pour que le système démarre, on veut que La transconductance critique $gm_{crit}$ du système soit comprise dans cet intervalle.$$ g_{m_{crit}}=4*ESR*(2*\pi*f_r)^2*(C_0+C_L)^2 $$
Notre choix s'est orienté vers le quartz [FL4000205Z](https://www.digikey.fr/fr/products/detail/diodes-incorporated/FL4000205Z/6049194) produit par Diodes Incorporated. Le tableau 1 récapitule ses principales caractéristiques.

| Caractéristique                    | Valeur      |
| ---------------------------------- | ----------- |
| Fréquence de résonance $f_r$       | $40 MHz$    |
| Résistance équivalente série $ESR$ | $40 Ohm$    |
| Capacité de charge $C_L$           | $10 pF$     |
| Capacité de shunt $C_0$            | $5 pF$      |
| $g_{m_{crit}}$                     | $2274 µA/V$ |
Tableau 1 : Caractéristiques du Quartz FL4000205Z

La transconductance critique du quartz est comprise dans l'intervalle conseillé par le constructeur. Le système devrait **EN THEORIE** démarrer.
#### Condensateurs de charge
Les condensateurs $C_1$ et $C_2$ placés autour du quartz doivent avoir une capacité équivalente à la capacité de charge $C_L$ spécifié par le quartz. La capacité de charge est exprimée par l'équation $$C_L=\frac{C_1*C_2}{C_1+C_2}+C_S$$
On note l'existence d'une capacité parasite $C_S$ entre les pattes du quartz. Cette capacité ne peut pas être négligée car elle est de l'ordre de quelques $pF$ , comme les autres capacités. En considérant une capacité $C_S=3pF$, on choisit $$C_1=C_2=20pF$$
Il faudra obligatoirement tester ce jeu de valeurs en plus d'autres valeurs relativement proches ($16pF$, $18pF$, $20pF$,  $22pF$ et $24pF$) pour obtenir la meilleure stabilité possible.
#### Résistance en série
Dans certains cas, elle peut être requise pour limiter le courant dans l'oscillateur. On ne l'utilise pas ici.
## Sources
- [AN2867 Rev 19](https://www.st.com/resource/en/application_note/cd00221665-oscillator-design-guide-for-stm8af-al-s-stm32-mcus-and-mpus-stmicroelectronics.pdf)
- [FL4000205Z datasheet](https://www.diodes.com/assets/Datasheets/FL.pdf)
- [MCP2518FD datasheet](https://ww1.microchip.com/downloads/aemDocuments/documents/OTH/ProductDocuments/DataSheets/External-CAN-FD-Controller-with-SPI-Interface-DS20006027B.pdf)