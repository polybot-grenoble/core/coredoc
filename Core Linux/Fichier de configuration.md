Les principaux paramètres de Core peuvent être modifiés à partir de son fichier de configuraiton. Ce fichier est automatiquement généré au démarrage de Core.

Ce fichier est disponible dans un dossier à la racine de votre compte utilisateur : `~/.core/config.json`.

Chaque fichier de configuration suit la structure ci-dessous :
```json
{
    "cockpit": {
        "hmiAddress": "127.0.0.1",
        "hmiPort": 42012,
        "port": 42042
    },
    "hermes": {
        "can-iface": "vcan0",
        "id": 144,
        "udp-clients": [
            {
                "id": 15,
                "ip": "127.0.0.1",
                "port": 11000
            }
        ],
        "udp-port": 42069
    },
    "lua": {
        "families": {
            "gamepad": "/path/to/peripherals/gamepad.lua",
            "pegasus": "/path/to/pegasus.lua"
        },
        "libraries": [],
        "peripherals": {
            "15": "gamepad",
            "42": "pegasus"
        },
        "strategies": [
            "/path/to/strats/"
        ]
    },
    "stratManager": {
        "backupMode": "SIMPLE",
        "maxRetry": 3,
        "timeBetweenRetryMs": 333,
        "timeout": 15
    }
}
```
## `cockpit`
- `hmiAddress`
- `hmiPort`
- `port`
## `hermes`
- `can-iface`
- `id`
- `udp-clients`
- `udp-port`
## `lua`
- `families`
- `libraries`
- `peripherals`
- `strategies`
## `stratManager`
- `backupMode` Mode qui régit le fonctionnement de du gestionnaire en cas de perte de la stratégie en cours.
	- `SIMPLE` Le gestionnaire tente de sélectionner la stratégie de secours spécifiée dans la stratégie perdue.
	- `AUTO` Le gestionnaire sélectionne la prochaine stratégie dont l'ensemble des périphériques est disponible.
- `maxRetry` Nombre de tentatives de restoration d'une stratégie avant que celle-ci soit considérée comme perdue.
- `timeBetweenRetryMs` Durée en milisecondes entre chaque tentative de restoration d'une stratégie. Cette durée doit être supérieure à la plus grande durée d'initialisation d'un périphérique.
- `timeout` Durée en seconde avant la fin automatique d'une stratégie. Dans le cas de la Coupe de France, cette durée équivaut à celle d'un match, soit 100 secondes.

D'autres paramètres sont suceptibles de faire leur apparition au fûr et à mesure du développement de Core.
