Auteur : Baptiste Maheut
Version : 1
![](Pasted%20image%2020240402222204.png)
## Introduction
L'objectif du SPI-CAN Controller est de fournir un support physique CAN à un ordinateur embarqué Raspberry PI4. La carte a été conçue autour du contrôleur MCP2518FD et du transmetteur MCP2542FD. Le contrôleur CAN est cadencé par un quartz de 20MHz et s'interface avec une liaison SPI.

Dans l'état actuel, ce PCB est **fonctionnel**.

**NB:** La tension logique de la Raspberry Pi 4 est de 3.3V. Le transmetteur MCP2542FDT-E/SN utilisé sur ce PCB n'est donc pas le même que celui utilisé par le [CAN Transceiver](https://gitlab.com/polybot-grenoble/pcb/prototype-can-transceiver-split.
## Fonctionnalités
- Support physique pour le CAN
- Port I2C
- Ports GPIO
- Ports Serie avec adaptateur de niveau de tension (3.3V et 5V)
- Connecteur pour ventilateur
## Améliorations possibles
- Reculer les connecteurs vers l'intérieur de la carte pour améliorer leur résistance aux chocs et limiter les déconnexions intempestives.
- Remplacer les résistances 1206 WIDE par des résistances au format plus standard.
- Remplacer les capacités céramique 0805 par des 1206, plus simple à manipuler.
- Réutiliser le plus possible les composants entre les différents PCB.
- Retirer l'UART de secours pour le dédier à l'UART de l'IHM. Le BCM2835 ne peut utiliser qu'un port UART à la fois...
## Nomenclature (BOM)
| Marqueur   | Composant                                                                                                             | Quantité |
| ---------- | --------------------------------------------------------------------------------------------------------------------- | -------- |
| U1         | [MCP2518FDT-E/SL](https://www.digikey.fr/fr/products/detail/microchip-technology/MCP2518FDT-E-SL/10231313)            | 1        |
| Y2         | [Crystal 20MHz 3225 4 pins](https://www.digikey.fr/fr/products/detail/abracon-llc/ABM8G-20-000MHZ-18-D2Y-T/2218017)   | 1        |
| C1, C2     | [Capacité céramique 1206 50V 18pF](https://www.digikey.fr/fr/products/detail/kyocera-avx/08056D104KAT2A/11568656)     | 2        |
| U3         | [MCP2542FDT-E/SN](https://www.digikey.fr/fr/products/detail/microchip-technology/MCP2542FDT-E-SN/5975416)             | 1        |
| C5         | [Capacité céramique 1206 50V 4700pF](https://www.digikey.fr/fr/products/detail/kemet/C1206C472J5RAC7800/2215931)      | 1        |
| C3, C4, C6 | [Capacité céramique 0805 6.3V 0.1uF](https://www.digikey.fr/fr/products/detail/kyocera-avx/08056D104KAT2A/11568656)   | 3        |
| R2, R3     | [Résistance 1206 WIDE 1.5W 60Ohm](https://www.digikey.fr/fr/products/detail/rohm-semiconductor/LTR18EZPF60R4/4052953) | 2        |
| R1, R4, R5 | Résistance 1206 10kOhm                                                                                                | 3        |
| J5, J6     | [JST S2B-PH-K](https://www.digikey.fr/fr/products/detail/jst-sales-america-inc/S2B-PH-K-S/926626)                     | 2        |
| J4, J10    | [JST S3B-EH](https://www.digikey.fr/fr/products/detail/jst-sales-america-inc/S3B-EH/926534)                           | 2        |
| J1, J2, J7 | Pins femelle 1x2 2.54mm                                                                                               | 3        |
| J3         | Pins femelle 1x3 2.54mm                                                                                               | 1        |
| J9         | Pins femelle 1x6 2.54mm                                                                                               | 1        |
| J8         | Pins femelle 1x5 2.54mm                                                                                               | 1        |
Ce document n'est pas définitif et les composants listés ci-dessus peuvent être modifiés sans préavis.
## Ressources
- [Documentation technique MCP2518FD](https://ww1.microchip.com/downloads/en/DeviceDoc/External-CAN-FD-Controller-with-SPI-Interface-DS20006027B.pdf)
- [Manuel de référence MCP2518FD](https://ww1.microchip.com/downloads/en/DeviceDoc/MCP25XXFD-CAN-FD-Controller-Module-Family-Reference-Manual-DS20005678E.pdf)
- [Documentation technique Quartz](https://abracon.com/Resonators/ABM8G.pdf)
- [Documentation technique MCP2542FD](https://ww1.microchip.com/downloads/en/DeviceDoc/MCP2542FD-MCP2542WFD-4WFD-Data-Sheet-DS20005514C.pdf)
- [Accès CADLAB](https://cadlab.io/project/27742/main/files)
- [Accès Gitlab](https://gitlab.com/polybot-grenoble/pcb/prototype-can-controller-pi)
- [Documentation principale](https://gitlab.com/polybot-grenoble/core/coredoc)