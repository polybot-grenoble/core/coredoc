La majeure partie du code du robot est exécuté sur un ordinateur embarqué. Dans notre cas, on utilise le Raspberry Pi 4B+. Dans l'ensemble des documents ci-dessous, Raspberry Pi 4 est abrégé Pi4.
Pour plus de clarté, le sommaire ci-dessous organise chronologiquement l'installation du Pi4 pour permettre une mise en route rapide et efficace.
## Sommaire
- [Présentation du Pi4](##Présentation du Pi4)
- [Installation de l'OS](Installation%20de%20l'OS.md)
- [Configuration Temps Réel](Configuration%20Temps%20Réel.md)
- [Installation des paquets](Installation%20des%20paquets.md)
- [Recherches contrôleur](Recherches%20contrôleur.md)
- [Bugs connus](Bugs%20connus.md)
## Présentation du Pi4

**NB :** Les GPIO sont limités à 3.3V. Des tensions logiques de 5V détruisent les GPIO.

## Bugs connus
