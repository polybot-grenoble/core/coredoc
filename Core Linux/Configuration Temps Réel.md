## Introduction
Nous souhaitons pouvoir configurer des thread en temps réel. Cela requiert que Linux soit modifié pour qu'il fonctionne en Temps Réel.
Tout au long de ce document, Raspberry Pi 4 sera abrégé en Pi4.

Plusieurs solutions s'offrent à nous :
- Patcher le Kernel avec `PREEMPT_RT`
- Configurer Xenomaï sur le Raspberry Pi 4

Xenomaï peut être une bonne solution mais elle est plus complexe à mettre en place. Patcher le Kernel avec `PREEMPT_RT` permet de modifier la préemption de celui-ci.
Un kernel `FULLY_PREEEMPTIBLE` permet de définir la priorité de chaque thread, et d'en définir en tant que temps réel. Cela a pour effet de réduire leur jitter lorsque le système est en pleine charge.

Notre choix s'est donc orienté sur le patch `PREEMPT_RT`.

Ce procédé n'est pas très complexe mais il requiert de choisir la bonne version du patch qui doit correspondre exactement à la version du kernel.

La dernière version en date de notre système est basée sur [Linux 6.1](https://github.com/raspberrypi/linux/tree/rpi-6.1.y).
Le patch utilisé est le suivant : [patches-6.1.59-rt16.gz](https://mirrors.edge.kernel.org/pub/linux/kernel/projects/rt/6.1/patch-6.1.59-rt16.patch.gz)
**NB:** La version du patch va changer avec le temps, toutes les versions sont disponibles [ici](https://mirrors.edge.kernel.org/pub/linux/kernel/projects/rt/6.1/).

Le kernel peut prendre plusieurs heures à compiler sur le Pi4. Le tutoriel suivant a été écrit pour permettre de cross-compiler le kernel, c.à.d. de le compiler sur sa machine personnelle, puis de le transférer au Pi4.
## Patch
En premier lieu, on configure le dossier puis applique le patch `PREEMPT_RT`.
```shell
mkdir rpi-kernel-rt
cd rpi-kernel-rt
mkdir cargo

git clone --depth=1 https://github.com/raspberrypi/linux
git checkout rpi-6.1.y

wget https://mirrors.edge.kernel.org/pub/linux/kernel/projects/rt/6.1/patch-6.1.59-rt16.patch.gz

cd linux

zcat ../patch-6.1.59-rt16.patch.gz | patch -p1
```

L'opération se déroule bien si aucun message d'erreur et aucune question n'est posée.
## Compilation
On s'intéresse ensuite à la configuration du kernel.
```shell
KERNEL=kernel8
ARCH=arm64
make CROSS_COMPILE=aarch64-linux-gnu- bcm2711_defconfig

make menuconfig
```
On sélectionne : `General setup -> Preempt -> (x) FULLY_PREEMPTIBLE`
Tabassez `EXIT` pour sauvegarder et quitter l'utilitaire de configuration.

Enfin, on compile le kernel.
```shell
make -j4 Image modules dtbs
```
L'option `-j4` permet de choisir le nombre de cœurs que vous dédiez à la compilation. Adaptez ce nombre en fonction de votre machine pour réduire le temps de compilation.
## Compression
Il faut maintenant installer le kernel et ses modules. 2 options s'offrent à nous :
1. Copier les fichiers directement sur la carte SD du Pi4.
2. Compresser les fichiers dans une archive à destination du Pi4.

L'option 1 est pratique si on a le RPI4 sous la main et un adaptateur micro SD. Si ce n'est pas le cas, on applique la seconde méthode.

```shell
sudo env PATH=$PATH make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- INSTALL_MOD_PATH=PATH/TO/rpi-kernel-rt/cargo/ modules_install

sudo cp arch/arm64/boot/Image ../cargo/$KERNEL.img

# Le tout dans une archive, facilement transportable (FTP, SCP, CLE USB)
cd ../cargo
tar czf ../kernel-rt.tgz * 
```
## Installation
Une fois l'archive récupérée sur le Pi4, on peut mettre à jour le kernel.

```shell
sudo cp /boot/kernel8.img /boot/kernel8.img.bak #BACKUP IMPORTANTE

cd /tmp
tar xvf kernel-rt.tgz

cd lib
sudo cp -rd * /lib/

sudo cp kernel8.img /boot/
```

Il ne reste plus qu'à modifier le fichier `/boot/config.txt` :
```shell
kernel=kernel8.img
```

On n'oubliera pas de redémarrer le Pi4 pour que le changement opère. Si ça ne boot pas, il faut restaurer le kernel précédent et modifier `/boot/config.txt`.
*GLHF* pour debugger.

Pour vérifier que le kernel soit en place, `uname -a` doit afficher la version compilée et également le suffixe `PREEMPT_RT`.
## Sources
Le tutoriel ci-dessus est basé sur les articles suivants :
- https://www.raspberrypi.com/documentation/computers/linux_kernel.html#building
- https://www.jeffgeerling.com/blog/2020/cross-compiling-raspberry-pi-os-linux-kernel-on-macos
- https://forums.raspberrypi.com/viewtopic.php?t=344994
- https://lemariva.com/blog/2021/01/raspberry-pi-4b-preempt-rt-kernel-419y-performance-test