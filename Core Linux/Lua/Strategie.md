
Cette section détaille comment écrire une stratégie pour Core en Lua.

## Qu'est-ce qu'une stratégie ?

## Les champs obligatoires

## Exécution d'une stratégie

## Ecrire une stratégie

### Les variables et les méthodes propres à l'objet `Strategie`

**NB :** Lorsque vous écrivez une méthode au sein d'une stratégie, remplacez `strat` par `self`.
#### `strat:init()`
Fonction d'initialisation de la stratégie. Cette méthode est définie par l'utilisateur et est **obligatoire**.
```lua
function strat:init ()
	-- Strat init code 
end
```
#### `strat:panic()`
Fonction d'initialisation post erreur de la stratégie. Cette méthode est définie par l'utilisateur et est **facultative**.
```lua
function strat:panic ()
	-- Strat panic code
end
```
#### `strat:loop()`
Fonction exécutée à chaque tick de Core. Cette méthode est définie par l'utilisateur et est **facultative**.
```lua
function strat:loop ()
	-- Strat loop code
end
```
#### `strat:main()`
Fonction représentant le programme principal de la stratégie. Cette méthode est définie par l'utilisateur et est **obligatoire**.
```lua
function strat:main ()
	-- Main strat code
end
```
#### `strat["key"]`
Accéder à une variable contenue dans stratégie peut retourner :
- Un périphérique en cache (voir `required_peripherals`)
- Une variable créée par l'utilisateur
- `nil`
```lua
function strat:main ()
	local base_mobile = self["pegasus:12"] -- Gets a peripheral
	...
	self.truc = 42
	...
	a = a + self.truc -- Gets a variable
	...
	local k = self.coucou -- Gets nil
	...
end
```
#### `strat.name`
Le nom de la stratégie. Il doit être **unique** et **obligatoirement** défini par l'utilisateur. Un nom vide (i.e. `""`) est invalide.
```lua
strat.name = "Nada"
```
#### `strat.fallback`
Le nom de la stratégie qui doit être appelée si la stratégie actuelle échoue. Ce champ est **facultatif**.
```lua
strat.fallback = "Guillaume"
```
#### `strat.team`
Le numéro de l'équipe pour laquelle s'applique la stratégie. Une valeur de 0 est **invalide**. Ce champ est **obligatoire**.
```lua
strat.team = 12 -- IEEESE!!!!
```
#### `strat.required_peripherals`
Tableau décrivant les `id` des périphériques requis par la stratégie, qui sont mis en cache à l'exécution.
```lua
strat.required_peripherals = { "pegasus:12", "wario_eats_shampoo:123" }
```
#### `strat:log(message)`
Affiche un message de log dans la console. 
Les arguments sont :
- `message` : (string) Le message à afficher dans la console
```lua
function strat:init ()
	self.n = 12
	self:log("J'ai " .. tostring(self.n) .. " en tant que valeur de n")
end
```
#### `strat:requirements_met()`
Retourne un booléen pour savoir si la stratégie peut mettre en cache l'ensemble de ses périphériques requis.

#### `strat:sleep(time)`
Bloque la stratégie pour `time` millisecondes.