Cette section détaille comment écrire un périphérique pour Core Linux en Lua.

## Qu'est-ce qu'un périphérique Lua ?

Un périphérique Lua est un script permettant d'écrire les abstraction pour envoyer des commandes ou reçevoir des informations à un périphérique physique. 

Le but est d'encapuler la création et la lecture des buffer Hermes dans des méthodes propres au périphérique pour rendre la rédaction des stratégies plus verbeuse.

## Les champs obligatoires

Il n'y a pas de champ obligatoire.

## Utilisation d'un périphérique

Un périphérique possèdant un identifiant Hermes `hermesID` (entier non signé sur 8 bits) est associé à une _famille_ de périphériques. 
Une famille de périphériques associe simplement un périphérique à son script. Cela permet de ne pas imposer un identifiant par script (ça évite la duplication de code), en plus de permettre d'avoir plusieurs fois le même type d'actionneur sur le robot.
 
Pour utiliser le périphérique, il faut déclarer l'existence du script dans le champ `families` du fichier `config.json`.

Ensuite il faut déclarer le périphérique physique dans le champ `peripherals` qui associe l'identifiant Hermes à la famille du périphérique.

## Ecrire un périphérique Lua

Pour rédiger les méthodes et assigner les valeurs par défaut des variables d'un périphérique, la variable globale `peripheral` est fournie.

Cette variable globale existe **uniquement** lors du chargement du script, pour référer au périphérique dans une méthode, on utilisera `self`.

Pour assigner une valeur à l'initialisation, on utilise la syntaxe :
```lua
peripheral.var = ...
-- ou
peripheral["var"] = ...
``` 

Pour déclarer une méthode, on utilise la syntaxe :
```lua
function peripheral:methode ()
	...
end
```

**NB :** En Lua, les définitions et appels de méthodes se font à l'aide de l'opérateur `:`, tandis que l'accès aux variables se fait par les opérateurs `.` et `[]`.

Pour modifier une variable durant un appel de méthode, on fera :
```lua
function peripheral:methode ()
	self.var = 12
end
```

Pour appeler une méthode au sein d'une autre méthode, on fera :
```lua
function peripheral:methode2 ()
	self:methode()
end
```

### Les variables et les méthodes propres à l'objet `Peripheral`

**NB :** On imaginera dans les exemples que le périphérique se trouve être une pomme de terre.
#### `peripheral["key"]`
Les types de variables pouvant être définies dans un périphériques sont :
- Booléens
- Nombres
- Chaines de caractères
- Méthodes
Tout autre type de donnée sera rejeté.
```lua
-- Définir une chaine de caractères
peripheral.k = "constante"
-- Définir un nombre
peripheral.x = 0.69
-- Définir un booléen
peripheral.est_une_patate = true
-- Définir une méthode
function peripheral:cuit ()
	peripheral:log("Pshhhit")
end
```
#### `peripheral:log(message)`
Affiche un message de log dans la console. 
Les arguments sont :
- `message` : (string) Le message à afficher dans la console
```lua
function peripheral:cuit ()
	peripheral:log("Pshhhit")
end
```
#### `peripheral:error(message)`
Affiche un message d'erreur dans la console.
Les arguments sont :
- `message` : (string) Le message à afficher dans la console
#### `peripheral:debug(message)`
Affiche un message de debug dans la console.
Les arguments sont :
- `message` : (string) Le message à afficher dans la console
#### `peripheral:send(buffer)`
Envoie un Buffer Hermes à son homologue physique.
Les arguments sont :
- `buffer` : (HermesBuffer) Le buffer à envoyer
```lua
-- Fait fleurir la patate
function peripheral:fleuri ()
	-- On crée un nouveau buffer
	local buffer = HermesBuffer.new()
	-- On le nettoie
	buffer:clear()
	buffer:rewind()
	-- On définit sa destination (même si vous vous trompez d'id, il sera écrasé)
	buffer:set_destination(self.hermesID, 1, false)
	-- On ajoute les arguments
	buffer:add_int8(1)

	-- On envoie le buffer
	self:send(buffer)
end
```
#### `peripheral:request(buffer, max_duration)`
Effectue une requête **bloquante** auprès de son homologue physique. 
Si la réponse est reçue en moins d'une `max_duration`, alors la fonction retourne le buffer reçu en tant que réponse. Sinon, la fonction retourne `nil`. Toutefois, si `max_duration = 0`, alors la requête est considérée
à temps infinie et n'est invalidée que si la stratégie est arrêtée de force.

Les arguments sont :
- `buffer` : (HermesBuffer) Le buffer à envoyer
```lua
-- Demande à la patate le taux d'humidité du sol
function peripheral:humidite ()
	-- On crée un nouveau buffer
	local buffer = HermesBuffer.new()
	-- On le nettoie
	buffer:clear()
	buffer:rewind()
	-- On définit sa destination (même si vous vous trompez d'id, il sera écrasé)
	buffer:set_destination(self.hermesID, 2, false)

	-- On effectue la requête
	local result = self:request(buffer, 2000) -- Timeout = 2s

	-- On vérifie qu'on a pas timeout
	if result == nil then
		self:error("La patate n'a pas répondu T_T")
		return nil
	end

	-- On récupère les données transmises
	local data = result:data()
	-- On vérifie qu'on a le nombre de données attendues
	if #data < 1 then
		self:error("La patate ne m'a pas donné le taux d'humidité")
		return nil
	end

	-- On retourne la donnée 
	-- (Oui, en lua l'indice des tableaux commence à 1 ...) 
	return data[1]

end
```
#### `peripheral.id`
(string) Identifiant du périphérique.
#### `peripheral.hermesID`
(nombre) ID Hermes du périphérique.
#### `peripheral.family`
(string) Famille du périphérique.
#### `peripheral:dump()`
(string) Retourne les variables stockées par le périphérique au format JSON.
#### `peripheral:heartbeat()`
Envoie un paquet de type Heartbeat à son homologue physique.
#### `peripheral:reset()`
Envoie un paquet de soft-reset à son homologue physique.

#### `peripheral.handlers`

_Cette fonctionnalité est actuellement désactivée._

(Ecriture seule) Fonctions executées à la réception d'un buffer en
provenance d'un périphérique mais qui n'a pas été demandé par une requête.

`handlers` est un tableau indexé par un entier (n° de la commande) et
associe à cet entier une fonction prenant en paramètres la référence 
vers le périphérique et le buffer reçu.

**NB :** Chaque assignation écrase **TOUT** handler déjà défini.

```lua
function handle_0 (peripheral, buffer)
	peripheral:log("J'ai reçu un buffer n°0 de manière inattendue !")
end

peripheral.handlers = {
	[0] = handle_0
}
```