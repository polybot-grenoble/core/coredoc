Voici la liste non-exhaustive des paquets installés sur la RPI4. Ces paquets peuvent être installés avec la commande suivante : `apt install <nom>`

- `vim` : Traitement de texte
- `rt-tests` : Utilitaire de tests pour le temps réel
- `stress-ng` : Autre utilitaire de tests pour le temps réel
- `can-utils` : Utilitaire de commandes liées au CAN
- `git` : Outil de versioning essentiel
- `build-essential` : Toolchain C/C++
- `cmake` : Outil de compilation
- `kernelshark` : Surveillance des évènements Kernel
- `nmtui` : Utilitaire wifi CLI