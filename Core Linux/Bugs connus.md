Plusieurs bugs ont été constatés lors de la phase de prototypage. Ces derniers sont documentés au mieux ci-dessous.
## Deuxieme démarrage sur le même pin INT0
### Environnement
[2-CH CAN HAT](https://www.waveshare.com/wiki/2-CH_CAN_HAT) qui utilise le MCP2515. INT0 connecté à GPIO23.
### Description
Lors du premier boot, le CAN va réussir à se setup (`dmesg`). Si on reboot (soft ou hard), même plusieurs fois, le CAN ne va pas réussir à se setup.
La seule solution trouvée à ce jour (21/12/23) est de modifier l'emplacement de INT0 sur la rasp, GPIO25 par exemple, de modifier le `/boot/config.txt` puis de rédemarrer. Le CAN va réussir à se setup, mais pour une unique session.
### Raison
Sequence de démarrage du controleur qui ne correspond pas à celle du driver dans certains cas d'allumage (soft ou hard).
### Solution
Désactiver le pilote (`rmmod`), Debrancher l'alimentation, Réactiver le pilote (`modprobe`), Rebrancher l'alimentation.
### NB
Bug inexistant sur les MCP2518FD, les puces utilisées sur les cartes actuelles.
## MCP2518FD bloqué dans l'état de configuration
### Environnement
Raspberry Pi 4B+, driver : mcp251xfd, puce : mcp2518fd
### Description
Le périphérique est correctement détecté au démarrage de la Raspberry Pi 4B+ (cf. `dmesg`). Lorsque que l'on active le socket can (cf. Installation), la puce reste en état de configuration. Le message est le suivant :
```mcp251xfd spi0.0 can0: Controller failed to enter mode CAN 2.0 Mode (6) and stays in Configuration Mode (4) (con=0x068b0760, osc=0x00000468)```
### Raison
Il y a de très grandes chances que ce bug soit causé par le transmetteur connecté au contrôleur qui considère que le bus est tout le temps occupé. C'est embêtant car l'état du contrôleur ne change que lorsque le bus n'est pas occupé (cf documentation contrôleur).
### Solution
Plusieurs cas existent :
- Aucun transmetteur n'est connecté (ex: test en loopback) : Court circuiter RX et TX.
- Un transmetteur est connecté : Celui-ci est probablement défectueux. Il faut tester avec un autre transmetteur.
## Aucun périphérique n'est détecté
Possibles raisons et solutions :
- Vérifier que les terminaisons sont connectées aux deux extremités du bus et pas sur les terminaisons sur un noeud au milieu.
- Vérifier que le can est up sur la Raspberry Pi 4.
- Vérifier la connexion entre les câbles du bus.
- Si rien ne change, éteindre la Raspberry Pi 4 `sudo shutdown now`, éteindre l'alimentation du robot, reconnecter l'alimentation.