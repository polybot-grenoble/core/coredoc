Ce document liste les commandes à executer pour utiliser Core, logiciel stocké sur la Raspberry Pi 4 du robot.
## Connexion SSH
**ATTENTION :** Etre sur le même wifi que celui de la Raspberry Pi 4.

`ssh polybot@raspberry.local`

Mot de passe : `polybot`
Utile : Importer votre clé ssh pour ne pas avoir à retaper le mot de passe à chaque fois.
## Activer l'interface CAN
Vérifiez si le CAN est activé : `ip a` -> ligne `can0` UP ou DOWN
- Si UP : Rien à faire, le CAN est allumé
- SI DOWN :
`sudo ip link set can0 type can bitrate 1000000 loopback off listen-only off`
`sudo ip link set can0 up`

Vérifier à nouveau avec `ip a`
- Si erreur ou DOWN -> `sudo dmesg | grep can`
	Si problème ou rien d'affiché -> GLHF c'est un pb matériel avec le shield de la Raspberry Pi 4. _Call me maybe._
## Mettre à jour Core
Des correctifs de Core ont pu être `push` sur Gitlab depuis la dernière utilisation. Pour les obtenir :

`cd ~/core` -> `git pull --recurse-submodules`

N'oubliez pas de compiler les changements avec la commande ci-dessous. Cela ne compile que `Core` et pas les autres executables.

`cd ~/core/build && make core -j4`

Si besoin, vous pouvez recharger la configuration CMake avec :

`cd ~/core/build && cmake ..`
## Démarrer Core
`cd ~/core/build` -> `./core`
**ATTENTION :** Ca ne fait qu'afficher les logs mais ils donnent des informations utiles.
Les logs sont également disponibles dans `cd ~/core/build/logs` au format texte ou JSON.
## Démarrer Cockpit
Cockpit permet d'envoyer des commandes à Core sans passer par l'IHM de Nico le bg. Pas obligatoire à la coupe mais pratique s'il y a un bug

`cd ~/cockpit-udp-py` -> `python3 cli.py`
Port : `42012`
Nom : `hmi`

Ne pas hésiter à lancer cockpit dans un autre terminal avec une session ssh
## Démarrer une strategie
Depuis le client cockpit démarré dans l'étape précèdente.
### Obtenir de l'aide
Tapez `help` pour obtenir la liste des commandes utilisables avant d'envoyer un message à Julien ou Baptiste.
### Selectionner une équipe
`strat set_team 1` ou `2` en fonction de la couleur de l'équipe.
PENSEZ A VOUS METTRE D'ACCORD CAR CELA MODIFIE LES STRATEGIES UTILISABLES.
### Afficher les stratégies
`strat list`
Affiche les stratégies disponibles en fonction de votre équipe et des périphériques détectés.
### Choisir une stratégie
`strat select nom_de_la_strat`
Evitez d'utiliser des espaces sinon il faut ajouter des guillemets autour du nom.
### Charger la stratégie
`talos start`
**ATTENTION :** Vous ne pouvez plus changer de stratégie
### Initialiser la stratégie
`talos tirette_on`
Revient à enfoncer la tirette dans la fente. Le robot doit executer le `init` de la stratégie.
### Démarrer la stratégie
`talos tirette_off`
On tire la tirette, le robot devrait normalement executer le `main` de la stratégie.
Vous n'avez plus le choix que d'attendre la fin du match (`timeout` de strategie manager), la fin de la stratégie ou bien un appui sur l'arrêt d'urgence (dommage).
### Fin du match
A la fin du match, ne pas oublier la commande `talos reset` pour revenir en état `IDLE`. Vous pouvez connaitre l'état de `Core` avec la commande `talos show`. Vous revenez alors à l'état initial et une nouvelle stratégie peut être sélectionnée.
### Mise à jour des fichiers Lua
Si vous mettez à jour les fichiers de stratégie, vous pouvez executer la commande :
`strat reload`

Il en est de même pour les périphériques :
`peripheral reload`

Cela vous évite de redémarrer `Core`.
## Surveiller les messages Hermes
Vous pouvez lancer un executable qui affiche les messages Hermes en temps réel sur le CAN.
`cd ~/core/build` -> `./hermes-monitor can0`
## Editer le fichier de configuration
Le fichier de configuration de Core se trouve ici :
`~/.core/config.json`
Plus de détails concernant ce fichier sont disponibles [ici](Fichier%20de%20configuration.md)
## Editer les stratégies
A cette date (Avril 2024) les stratégies et périphériques Lua sont stockés dans le registre Gitlab [robot-lua](https://gitlab.com/polybot-grenoble/robot-lua).
Si vous n'êtes pas à proximité du robot, vous pouvez cloner ce registre sur votre PC ainsi que [Core](https://gitlab.com/polybot-grenoble/core/core) afin de faire des tests en local.
Si vous êtes à proximité du robot, il est conseillé de se connecter avec le mode *Connexion distante* de VSCode et d'accéder au dossier `robot-lua` dans la racine de la Raspberry Pi 4. Rechercher sur internet si vous n'y arrivez pas, on ne va pas vous nourrir à la cuillère.
## Procédure d'arrêt de la Raspberry Pi 4
Avant de débrancher l'alimentation, il est **primordial** d'éteindre la Raspberry Pi 4. Pour cela, il faut se connecter en SSH si ce n'est pas déjà fait puis executer la commande :
`sudo shutdown now`.
Vous pouvez débrancher l'alimentation une fois que les LED de la Raspberry Pi 4 ne clignotent plus (absence d'activité). Si vous ne la débranchez pas correctement, vous vous exposez à risque de corruption mémoire et une réinstallation complète du système d'exploitation, soit plusieurs heures de perdues.