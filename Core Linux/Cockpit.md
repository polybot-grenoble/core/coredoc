
Cockpit est le protocole de communication entre Core et les utilisateurs. Comme son nom l'indique, il est utilisé pour contrôler l'application et ses fonctionnalités.

Il s'agit d'une communication en JSON sans support prédéfinit. On peut donc l'utiliser sur un socket ~~UNIX, TCP ou WebSocket~~ UDP dans l'état actuel. La communication est en quelque sorte asynchrone: la réponse à une requête n'est pas immédiate, ni nécessairement dans l'ordre de celles-ci. De plus des paquets peuvent être envoyés à un client sans qu'il n'en fasse la demande.
Les commandes sont organisées sous formes d'arbres.

Les implémentations C++ et ~~TypeScript~~ Python seront détaillées dans des documents distincts.

## Paquet Cockpit

Le paquet de communication est structuré de la manière suivante en TypeScript:

```ts
type Packet = {
	uid:     string,
    client:  string,
    type:    string,
    payload: string
};
```

ou en C++:
```cpp
struct Packet {
	string uid;
	string type;
	string payload;
	string client;

	json raw ();
	bool from (json data);
};
```

Ce paquet contient les éléments essentiel pour le bon fonctionnement de la communication:
- `uid` (unique identifier) : Identifiant unique de requête (peut être vide)
- `type` : Type de paquet
- `payload` : Le contenu du paquet - Peut être n'importe quoi tant que sérialisé sous forme de texte
- `client` : Le client à qui est destiné le paquet

Le champ "client" peut paraître contre-intuitif ici vu que l'on veut contrôler un unique robot, mais l'ambition de ce protocole est de permettre le contrôle de Core via plusieurs moyens pour ne pas se retrouver coincés en cas de défaillance de l'un d'eux.

## Transmission de paquets

Pour transmettre un paquet, rien de plus simple !

Il suffit de sérialiser le JSON sous forme de string, puis de lui coller un caractère nul en fin de chaîne pour éviter les collisions de paquets, pour enfin l'écrire sur le socket choisi.

## Requêtes

Le paquet de type `request` contient une commande en tant que `payload`.

Cette commande est composée de mots séparés par des espaces, par exemple :
- `help`
- `vitals get`
- `robot goto 12 12`
- `settings set "TCP Port" 12500`
-  `json parse "{a: 1, b: \"str\"}"`

Un mot est constitué soit :
- d'une chaine continue de caractères alphanumériques
- d'une chaine de caractères contenues entre guillemets `"..."` ou `'...'`

Lorsque plusieurs types de guillemets sont utilisées, c'est la première rencontrée qui est définie comme attendue en fermeture:
- `test "str: 'bonjour'"` contient deux mots: `[ "test", "str: 'bonjour'" ]`

Attention : la détection de mots entre guillemets s'arrête lors de la première guillemet similaire non échappée:
- `say 'C'est une arnaque'` est erroné : le résultat sera `[ "say", "C", "est une arnaque'" ]` (dans le meilleur des cas)
- `say 'C\'est une arnaque'` fonctionne : le résultat sera `[ "say", "C'est une arnaque" ]`

## L'arborescence

Les requêtes représentent des chemins au sein d'un arbre. En effet, Cockpit va décomposer la commande en mots, ces mots seront utilisés pour router la requête à la bonne commande. Cela permet d'avoir une structure des commandes dynamique et simple à implémenter.

Prenons comme exemple de l'arbre suivant:

```
(root)
+ (vitals)
+ (robot)
	+ (CAN)
		+ help
		+ send <dest> <command> [data]
	+ (strat)
		+ help
		+ start <name> [force]
		+ stop
	+ help
+ help
+ logo
+ add <num1> <num2>
```

Chaque élément entre parenthèses `(...)` représente un routeur, ceux-ci sont imbriqués pour former des routes. Chaque route se termine par une commande, cette commande peut nécessiter des arguments obligatoires `<...>` ou optionnels `[...]`. Ceux-ci sont tous transmis sous forme de chaines de caractère, libre à celui qui implémente une commande de décider comment traiter ses arguments.

Dans la commande `robot CAN send 12 25 f6`, la route est `[ "robot", "CAN", "send" ]` et les arguments sont `[ "12", "25", "f6" ]`.
## Réponses

Le type d'une réponse dépend entièrement de la commande invoquée, il n'y a pas de règle stricte, seulement quelques définitions pour permettre la gestion des erreurs et des logs.
### Comportement par défaut

La réponse par défaut est un paquet de type `response`, le type du contenu est une chaine de caractères à renvoyer à l'utilisateur. Si vous souhaitez envoyer autre chose, définissez un autre type de paquet.

### Erreurs

Les paquets de type `error` ont une charge utile de la forme :
```json
{ "code": 0, "message": "" }
```

Le code d'erreur est défini selon une énumération, commune aux différentes implémentations de Cockpit.

On retrouve les plages suivantes :

| Plage | Signification | Exemples |
| ------ | ------------ | -------- |
| 0 à 99 | Erreur de traitement | Conversion, erreur inconnue, ... |
| 100 à 199 | Erreur de requête | Non trouvée, non implémentée, ... |
| 200 à 299 | Erreur à l'execution | Crash, typo, ... |
| >= 1000 | Erreur côté client | |

### Logs

Il existe 3 types de paquets de logs :
- `log` : Message informatif
- `log_debug` : Message à caractère optionnel
- `log_error` : Message d'erreur

Pour ces trois types de paquets, le message est contenu sous forme de texte dans le contenu du paquet. 

Pourquoi envoyer des paquets plutôt que de logger dans un fichier ? 
La raison est simple, l'implémentation de Cockpit en C++ utilise l'entrée et la sortie standard pour communiquer, il est donc inconcevable de mettre des logs dans celle-ci. De plus envoyer des paquets permet de les redistribuer à une multitude de clients connectés simultanément en plus de pouvoir dater les logs et les stocker au format JSON pour pouvoir les filtrer par sévérité plus facilement.
## Implémentations

Cockpit fonctionne de façon similaire mais avec quelques nuances dans les différents langages. 
La plus grosse différence est la façon dont est gérée une requête.
### ~~TypeScript~~

~~Basée sur Node.JS, cette implémentation tire parti de la syntaxe asynchrone native en utilisant des promesses. Cela permet de traiter plusieurs demande en parallèle sans avoir à gérer manuellement des threads.~~

~~Les requêtes contiennent un référence vers l'objet global Cockpit encapsulant les références vers l'ensemble des objets pouvant être utile pour traiter une demande. Cela permet d'éviter l'utilisation de variables globales.~~

~~De plus deux méthodes sont attachées à une requête : `respond` qui permet de répondre à la requête, et `erreur` qui permet d'envoyer une erreur à l'utilisateur ayant fait la requête.
Ces méthodes **envoient** le message, contrairement à l'implémentation C++.~~

### C++

~~La structure d'une requête en C++ est un peu plus complexe que son homologue en TypeScript pour faciliter son utilisation, et s'adapter aux limites du langages.~~

La requête est capable de générer des paquets de réponse, dont les erreurs de requêtes, et appelle Cockpit de les envoyer à l'aide d'une _fonction lambda capturante_.

La requête en C++ permet aussi d'obtenir le nombre d'arguments restants dans la commande en cours.

L'implémentation de Cockpit pour Core **ne permet pas d'effectuer de requêtes**. Il s'agit de l'hôte hébergeant les commandes.

### Python

Il existe un client Python créé afin d'utiliser Python sur UDP pour la révision de Mars 2024. En effet, pour simplifier l'intégration de Core, Cockpit communique désormais via UDP.

Les explications peuvent être trouvées dans le [Readme](https://gitlab.com/polybot-grenoble/core/cockpit-udp-py) du projet correspondant.