Suite à une requête de certains projets portés en 2024, comme le Lidar, nous avons décidé de concevoir une carte électronique d'adaptation pour utiliser les modules [CAN Transceiver](Core%20STM/CAN%20Transceiver.md) avec des cartes Nucleo-F446Re.

Nous avons décidé de ne pas commander de PCB chez notre fournisseur mais de réaliser un prototype à partir d'une carte à trous car cela doit rester une exception. L'encombrement des NUCLEO-F446RE est important dans un robot.
## Adaptation matérielle
Le schéma ci-dessous explicite les pins utilisés par le CAN et le Lidar. En réalité, le CAN n'a besoin que des pins D14, D15 et bien évidemment le +5V et la masse. Les pins A0 et A1 étaient réservés au Lidar.
![](Pasted%20image%2020240314093202.png)
En jaune : les pins utilisés par le shield d'adaptation.
## Adaptation logicielle
Le logiciel Core STM utilisé sur les L4 n'est pas compatible avec les F4 car chaque cible embarquée est unique. Heureusement pour nous les HAL ST permettent d'abstraire certains détails d'implémentation propre à chaque cible. Le code de Core STM a été adapté pour les cibles L432KC, F446RE et également G431KB, plus de détails concernant l'adaptation logicielle sont disponibles à la page [Logiciel Core STM](Core%20STM/Logiciel%20Core%20STM.md).
## Etat actuel
Le shield, normalement rangé dans la boite *Core* dans Polybot, est **fonctionnel**. Il a été testé en réseau avec plusieurs STM32 L4.

Un **bug non identifié** persiste : La carte reste muette lorsque le réseau est composé d'une F4 et d'une Raspberry Pi 4 équipée de son [contrôleur CAN](Core%20Linux/SPI-CAN%20Controller.md). Le bug disparait lorsqu'une L4 est ajoutée. Il faudrait explorer ce bug plus en détail à l'aide d'un oscilloscope branché sur la paire différentielle. Nous soupçonnons un problème lié au [bit timing](Configuration%20du%20contrôleur%20CAN.md#Timing) de la F4 et/ou à sa capacité à prendre la main sur le réseau (lecture de 11 bits récessifs pour pouvoir émettre).