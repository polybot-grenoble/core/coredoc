Pour pouvoir communiquer sur le bus CAN, un composant externe aux microcontrôleurs est requis : le transceiver. Ce dernier sert d'interface physique entre le microcontrôleur et la paire différentielle du bus CAN, comme le montre la *figure 1*.

![](architecture_materielle_peripherique_can.png)
*Figure 1  : Schéma haut niveau de l'architecture matérielle d'un périphérique*

Pour permettre à chaque équipe d'implémenter rapidement un périphérique, deux solutions sont proposées :
- Une carte fille qui intègre l'ensemble des composants, prête à utiliser.
- Le schéma électrique de l'architecture, à insérer dans un projet KiCAD.
- 
### Todo list
Explorer Standard VS Split termination
Explorer SMD Pin Header
Transient voltage suppressor
Decoupling capacitors on VDD : check
MCP2544WFDT ou VP230. Différence, pourquoi on n'utilise pas ceux dans Polybot ?
LED : STATUS


______________________________
ATTENTION : ALIMENTER NUCLEO ENTRE 7V et 12V par VIN. Sinon pas assez de courant.
ST DATASHEET : UM1956
Pour plus tard : Alimenter par le +5V à la place ?
![](Pasted%20image%2020240328104947.png)
______________________________


### Ressources utiles
https://e2e.ti.com/blogs_/b/industrial_strength/posts/the-importance-of-termination-networks-in-can-transceivers
http://www.mindsensors.com/content/86-can-and-its-topology
https://www.digikey.fr/fr/blog/the-basics-of-the-controller-area-network
https://www-techniques-ingenieur-fr.sid2nomade-2.grenet.fr/base-documentaire/automatique-robotique-th16/systemes-d-information-et-de-communication-42397210/bus-can-s8140/aspects-materiels-s8140v2niv10004.html
https://www.ti.com/lit/ds/symlink/sn65hvd230.pdf
https://resources.altium.com/p/pcb-design-guidelines-using-tvs-diode-transient-protection


