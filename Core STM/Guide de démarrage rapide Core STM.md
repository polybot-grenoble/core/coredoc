This guide briefly describes how to setup a STM32 Core peripheral. It might be intriguing at first, but it is really just like using the Arduino Library with overlays. In fact, you can use Arduino functions and libraries as well as STM32 HAL functions.
## Software setup
Follow these steps to properly setup a new peripheral.
1. Download [platform.io](https://platformio.org/platformio-ide). Our architecture is based on it and it works like a charm.
3. Fork this repository and name it after the peripheral you are building.
4. Clone the newly created repository onto your device. `git clone --recurse-submodules <your-repo-link.git>`
2. Once cloned, you must setup the STM32 platform on the platform.io tab in VSCode.
3. Select your target by clicking the blue `default` selector at the bottom of VSCode.
4. Write your program in the `user.cpp`. **NEVER WRITE FILES THAT ARE IN THE CORE FOLDER.**
5. Build your program using `CTRL+ALT+B`.
6. Upload your program using `CTRL+ALT+U`. Make sure your target is plugged into your computer.
7. To setup the debug mode, go on the platformio panel in VSCode then Click `Quick Access > Debug > Start Debugging`. It will open and configure VSCode Debug panel. This has to be done the first time your using the debugger.
8. When debugging, access the VSCode debug panel and select `PIO Debug`. You can now access memory, registers, variables, call stack and breakpoints.
## Hardware setup
Plug a [CAN Transceiver](CAN%20Transceiver.md). If you are using a F446RE, please read [Adaptation STM32 F446Re](Adaptation%20STM32%20F446Re.md) first.

The integrated CAN controller of your target uses specific pins to interface the transceiver. Those pins are listed in the table below :

| Cible  | TX    | RX    |
| ------ | ----- | ----- |
| L432KC | `D2`  | `D10` |
| F446RE | `D14` | `D15` |
| G431KB | `TBD` | `TBD` |
You shall not reassign those pins otherwise it will not work.
## How to
Instead of writing your program in Arduino's predefined functions, you must write them in the corresponding functions, inside the provided `user.cpp` file.

| Arduino       | Core                          |
| ------------- | ----------------------------- |
| `void init()` | `void Talos_initialisation()` |
| `void loop()` | `void Talos_loop()`           |
When a error occurs inside the Core program, `void Talos_loop()` will be paused and `void Talos_onError()` will be executed instead. It allows the developer to define some emergency functions to be executed like stopping wheels, or even forcing a restart using `Core_hardReset()`,  `Core_softReset()` or `Core_softResetIfConnected()`.You can force the error mode using `Core_forceError();`.
If the error cleans itself (ex: the CAN link is up again), `void Talos_loop()` will start again and `void Talos_onError()` will stop. Be careful, when exiting the error state, `void Talos_initialisation()` will be **executed once again**. You might not want your pin to be assigned again. Make sure to use a one-time flag to avoid this case.
**NB:** `void Talos_onError()` is a looping function.
### Send a message over the CAN bus
## Receive a message from the CAN bus

## Prototyping
Developers can prototype their peripherals without being annoyed by the selfcheck mechanism or by being forced to connect to the CAN network. it allows developers to independently develop and test their peripherals.
You must perfom the following operations :
- In `plaformio.ini`, set `HERMES_CAN_MODE` to `CAN_MODE_SILENT_LOOPBACK`
- In `plaformio.ini`, set `CORE_LOG_SERIAL` if not set.
Don't forget to set `HERMES_CAN_MODE` back to `CAN_MODE_NORMAL` when your peripheral to reach the network.
## Learn more
If you have any questions, read [Logiciel Core STM](Logiciel%20Core%20STM.md) first. If you still have questions, feel free to reach Core's maintainers. If there are no maintainers, you can contact Julien Pierson, Baptiste Maheut or Jonathan Dumaz, fathers of Core.