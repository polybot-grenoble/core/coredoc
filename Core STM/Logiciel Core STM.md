Core STM combine la puissance des HAL ST, [STM32duino](https://github.com/stm32duino/Arduino_Core_STM32), [Hermes](Hermes.md) et [Talos périphérique](Talos%20périphérique.md) pour permettre aux amateurs de robotique de communiquer entre plusieurs microcontrôleurs et autres appareils sur un bus CAN tout en fournissant des fonctionnalités haut niveau. Le code source est disponible sur [Gitlab](https://gitlab.com/polybot-grenoble/core/can-prototype-stm32).
## Concept
### Arduino
| Arduino       | Core                          |
| ------------- | ----------------------------- |
| `void init()` | `void Talos_initialisation()` |
| `void loop()` | `void Talos_loop()`           |
### HermesCAN
HermesCAN est une classe abstraite dont l'objectif est de lier Hermes au bus CAN. L'implémentation du contrôleur CAN peut varier d'un microcontrôleur à l'autre d'où le choix d'une classe abstraite. Deux classes héritent actuellement de `HermesCAN` : `HermesCAN2B` et `HermesCANFD`.
```cpp
class HermesCAN {
protected:
	Hermes_t *hermesInstance;
public:
	HermesCAN(Hermes_t *hermes) : hermesInstance(hermes){};
	virtual HermesCANReturnCode setup() const = 0;
	virtual HermesCANReturnCode setupIT() const = 0;
	virtual HermesCANReturnCode processRX(uint32_t fifo) const = 0;
	virtual HermesCANReturnCode processTX() const = 0;
};
```
#### Réception
IT CAN -> AJOUT HERMES BUFFER -> TRAITEMENT HORS IT
#### Emission
AJOUT BUFFER -> TRAITEMENT SEQUENTIEL
### Déconnexion / Erreur
HEARTBEAT
STRUCTURE CODE SOURCE
## Fonctionnalités
### Selfcheck
Core *hates* infinite loops.
By design, such loops prevent the system from processing CAN related messages, therefore we had to implement a self check (SC) mechanism.
This mechanism is periodically checking if the system is stucked into a loop. We also use this mechanism to check if the CAN network is up when it's too quiet.
### Contrôle de la période d'éxecution
### Boucle prioritaire
In case you have functions that need to be running no matter Core's state(ex: wheels automation), they can be run in `void Core_priorityLoop()`.
## Structure du code
### Arborescence
### Define
Plusieurs define sont utilisés dans le fichier `platformio.ini`. L'ordre des define n'a pas d'importance car ils ont été ajouté au fur et à mesure du développement de Core.

| Define                        | Description                                                                                                                                                          | Valeur par défaut | Obligatoire (O/N) |
| ----------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------- | ----------------- |
| `HAL_CAN_MODULE_ENABLED`      | Active la librairie HAL CAN                                                                                                                                          |                   | O                 |
| `HERMES_MAX_BUFFER_LEN`       | Taille maximale en octet d'un buffer Hermes                                                                                                                          | 1024              | O                 |
| `HERMES_BUFFER_COUNT`         | Nombre de buffer Hermes                                                                                                                                              | 16                | O                 |
| `HERMES_CAN_MODE`             | Mode de fonctionnement du contrôleur CAN. Valeurs possibles :<br>`CAN_MODE_NORMAL`<br>`CAN_MODE_LOOPBACK`<br>`CAN_MODE_SILENT`<br>`CAN_MODE_SILENT_LOOPBACK`         | `CAN_MODE_NORMAL` | O                 |
| `DEVICE_ID`                   | Identifiant Hermes de la cible                                                                                                                                       | 0                 | O                 |
| `MOTHER_ID`                   | identifiant Hermes de Core Linux                                                                                                                                     | 1                 | O                 |
| `CORE_LOG_SERIAL`             | Affiche les messages envoyé avec `Core_log()` sur le terminal série                                                                                                  |                   | N                 |
| `CORE_SELFCHECK_WAIT_TIME_MS` | Durée maximale (en millisecondes) d'inactitvité du CAN et avant que `Talos_loop()` soit considéré *freeze*. Le cas échéant Core STM programme passe en état d'erreur | 1000              | O                 |
| `CORE_CAN_TIMEOUT`            | Durée maximale (en millisecondes) de déconnexion du CAN avant passage en état d'erreur                                                                               | 1000              | O                 |
| `HAL_I2C_MODULE_DISABLED`     | Désactive la librairie HAL I2C. **Obligatoire pour la cible F446RE**                                                                                                 |                   | N                 |
| `CORE_L432KC`                 | Indique que la cible est une L432KC. **Ne pas l'utiliser avec un autre define de cible**                                                                             |                   | O                 |
| `CORE_F446RE`                 | Indique que la cible est une F446RE. **Ne pas l'utiliser avec un autre define de cible**                                                                             |                   | O                 |
| `CORE_G431KB`                 | Indique que la cible est une GB431KB. **Ne pas l'utiliser avec un autre define de cible**<br>                                                                        |                   | O                 |

**NB1 :** Les define `HERMES_MAX_BUFFER_LEN` et `HERMES_BUFFER_COUNT` ont une influence directe sur l'empreinte de Core STM dans la RAM. Vous devez donc adapter ces paramètres en fonction de la cible et de l'application à développer. Vous pouvez aisément remplir la RAM avec des buffer Hermes.
## Compatibilité matérielle
L'entièrete du projet est basé sur l'utilisation de [CAN Transceiver](CAN%20Transceiver.md). L'interfacage du microncontrôleur avec ce transmetteur dépend de la cible utilisée. Les pins utilisés sont précisés dans le tableau ci-dessous :

| Cible  | TX    | RX    |
| ------ | ----- | ----- |
| L432KC | `D2`  | `D10` |
| F446RE | `D14` | `D15` |
| G431KB | `TBD` | `TBD` |
Vous ne devez en aucun cas utiliser ces pins sous peine que Core STM ne fonctionne pas comme prévu.

Si vous souhaitez rendre une nouvelle cible compatible, il faut d'abord s'assurer que les pins reliés au contrôleur CAN soient au même endroit que ceux de la Nucleo L432KC. Si ce n'est pas le cas, il faudra fabriquer un [shield d'adaptation](Adaptation%20STM32%20F446Re.md) similaire a celui réalisé pour la F446RE. Il faudra très probablement modifier le code source de Core STM. Les grandes lignes de cette opération sont précisées dans la partie [Nouvelle cible](Logiciel%20Core%20STM.md#Nouvelle%20cible).
## Nouvelle cible
PREREQUIS, MANUELS DE REFERENCE, COMPATIBILITE ARDUINO
Modifier HermesCAN, IT, platformio.ini, TIMER Selfcheck